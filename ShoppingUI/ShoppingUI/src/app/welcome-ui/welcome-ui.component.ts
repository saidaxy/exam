import { Component, OnInit } from '@angular/core';
import { TreeNode, MessageService } from 'primeng/api';
import { ViewEncapsulation } from '@angular/compiler/src/core';

@Component({
  selector: 'app-welcome-ui',
  templateUrl: './welcome-ui.component.html',
  styleUrls: ['./welcome-ui.component.css']
})
export class WelcomeUIComponent implements OnInit {
  data: TreeNode[];
  selectedNode: TreeNode;
  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.data = [{

              label: 'Hotel Booking service',
              expanded: true,
              data:`Book any room in any hotel with my service `,
              children: [
                {
                  label: 'Easy',
                  expanded: true,
                  data:`All you need is just few clicks`
                },{
                  label: 'Cheap',
                  expanded: true,
                  data:`All rooms are very cheap`
                },{
                  label: 'Beautiful',
                  expanded: true,
                  data:`Awesome, amazing and beautiful project as me`
                }
              ]
            }
          ]
        }

  onNodeSelect(event) {
    this.messageService.add({severity: 'info', summary: event.node.label , detail: event.node.data, life: 10000});
}
}
